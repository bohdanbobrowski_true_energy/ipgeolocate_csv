import csv
import os
import sys
import socket
import requests

IPGEOLOCATION_API_KEY = os.getenv("IPGEOLOCATION_API_KEY")


def main():
    print(f"IPGEOLOCATION_API_KEY={IPGEOLOCATION_API_KEY}")
    if len(sys.argv) > 1 and IPGEOLOCATION_API_KEY:
        csv_file_name = sys.argv[1]
        output = []
        with open(csv_file_name) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=",")
            row_count = 0
            for row in csv_reader:
                try:
                    socket.inet_aton(row[0])
                    ip_address = row[0]
                    url = f"https://api.ipgeolocation.io/ipgeo?apiKey={IPGEOLOCATION_API_KEY}&ip={ip_address}"
                    result = requests.get(url)
                    response_json = result.json()
                    new_row = [
                        ip_address,
                        response_json["continent_name"],
                        response_json["country_name"],
                        response_json["state_prov"],
                        response_json["city"],
                        response_json["district"],
                        response_json["zipcode"],
                        response_json["latitude"],
                        response_json["longitude"],
                    ]
                except KeyError:
                    print(response_json)
                    new_row = [
                        row[0],
                    ]
                except socket.error:
                    new_row = [
                        row[0],
                    ]
                print(new_row)
                output.append(new_row)
                row_count += 1
        with open(csv_file_name, "w") as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=",")
            for row in output:
                csv_writer.writerow(row)


if __name__ == "__main__":
    main()
