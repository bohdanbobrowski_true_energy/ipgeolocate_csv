# Localize ip's from given csv files

## Installation

Before running anything sign up on [ipgeolocation.io](https://ipgeolocation.io/) and get your API KEY.

Run following commands:

    git clone git@gitlab.com:bohdanbobrowski_true_energy/ipgeolocate_csv.git
    cd ipgeolocate_csv
    python3 -m venv venv
    echo "export IPGEOLOCATION_API_KEY=<api key from ipgeolocation.io>">venv/bin/postactivate
    source ./venv/bin/activate


## Prepare csv files

Script requires simple csv file with at least one column, and in first column it should contain IP address. If csv contains header - it will be skipped. All other columns will be replaced!

CSV file will be overwritten!


## Running

You can run single command:

    python localise_ips.py csv_filename.csv

or execute bash script, to run script over all csv files in a folder:

    ./run_all.sh


## Documentation

You can read more docs here: [ipgeolocation.io/documentation/ip-geolocation-api.html](https://ipgeolocation.io/documentation/ip-geolocation-api.html).
